#include "Goulotte.h"

void setup()
{
  // Pin setup
  //  Buttons
  pinMode(PARA_BUTTON, INPUT);
  pinMode(DESK_BUTTON, INPUT);
  pinMode(PLAY_BUTTON, INPUT);
  pinMode(STOP_BUTTON, INPUT);

  //  attachInterrupt(digitalPinToInterrupt(PARA_BUTTON), PLAY_, FALLING);
  //  attachInterrupt(digitalPinToInterrupt(DESK_BUTTON), PLAY_, FALLING);
  //  attachInterrupt(digitalPinToInterrupt(PLAY_BUTTON), PLAY_, FALLING);
  //  attachInterrupt(digitalPinToInterrupt(STOP_BUTTON), STOP_N_PREVIOUS, FALLING);

  //  Leds
  //pinMode(PARA_LED, OUTPUT);
  //pinMode(DESK_LED, OUTPUT);
  //pinMode(PLAY_LED, OUTPUT);
  //pinMode(STOP_LED, OUTPUT);

  // Display setup
  lcd.init();
  //lcd.cursor();
  //lcd.blink();
  //lcd.backlight();
  delay(100);
  lcd.clear();
  lcd.backlight();

  lcd.home();
  lcd.print("Jacominus, checks");
  lcd.setCursor(0, 1);
  lcd.print("********************");

  // If the Arduino is powering the WAV Trigger, we should wait for the WAV
  //  Trigger to finish reset before trying to send commands.
  delay(1000);

  // WAV Trigger startup at 57600
  wTrig.start();
  delay(10);

  // Send a stop-all command and reset the sample-rate offset, in case we have
  //  reset while the WAV Trigger was already playing.
  wTrig.stopAllTracks();
  wTrig.samplerateOffset(0);

  wTrig.masterGain(0);
  wTrig.setAmpPwr(false);

  // Enable track reporting from the WAV Trigger
  wTrig.setReporting(true);

  // Allow time for the WAV Trigger to respond with the version string and
  //  number of tracks.
  delay(100);

  // If bi-directional communication is wired up, then we should by now be able
  //  to fetch the version string and number of tracks on the SD card.
  if (wTrig.getVersion(wTrigVersion, VERSION_STRING_LEN))
  {
    SD_TRACKS = wTrig.getNumTracks();

    while (digitalRead(PLAY_BUTTON))
    {
      lcd.setCursor(0, 2);
      lcd.print("SDC = "); lcd.print(SD_TRACKS);
      lcd.print(" / ");
      lcd.print("PGM = "); lcd.print(NB_TRACKS);
      lcd.print("");

      lcd.setCursor(0, 3);
      lcd.print(wTrigVersion);

      lcd.setCursor(19, 3);
      lcd.print(">");
    }
    lcd.clear();
  }

  else
  {
    while (1)
    {
      lcd.clear();

      lcd.setCursor(0, 0);
      lcd.print("Error: WAV Trigger");

      lcd.setCursor(0, 1);
      lcd.print("not available");

      lcd.setCursor(0, 3);
      lcd.print("Exit");
    }
  }

  // Fake loop
  // -->
  lcd.home();
  lcd.print("Peparation de");
  lcd.setCursor(0, 1);
  lcd.print("l'interface ...");

  for (int i = 0; i < 20; i++)
  {
    lcd.setCursor(i, 3);
    lcd.print("*");
    delay(50);
  }
  // <--

  // Layout setup
  lcd.clear();
  lcd.home();
  lcd.print("##  ");
  lcd.print("[TRACK_LABEL]");
  lcd.setCursor(0, 2);
  lcd.print("#");
  lcd.setCursor(1, 2);
  lcd.print(LAYER + 1);
  lcd.setCursor(3, 2);
  if (LAYER < 9) {
    lcd.setCursor(2, 2);
    lcd.print("  ");
  }
  else {
    lcd.setCursor(3, 2);
    lcd.print(" ");
  }
  lcd.print(TRACK_LABEL[LAYER]);
  lcd.setCursor(19, 2);
  lcd.print(">");
}

// Loop
// ==================================================================================================

void loop() {
  wTrig.update();
  TIME = millis();

  if (!digitalRead(PARA_BUTTON) || !digitalRead(DESK_BUTTON) || !digitalRead(PLAY_BUTTON))
  {
    if (TOKENEXT)
    {
      LAYER++;

      TOKENEXT = false;
      LAST_PLAY_TIME = TIME;
    }
  }

  else if (!TOKENEXT && TIME - LAST_PLAY_TIME > SECURITY_DELAY && LAYER != NB_TRACKS)
  {
    TOKENEXT = true;
    lcd.setCursor(19, 2);
    lcd.print(">");
  }

  if (LAYER != LAST_LAYER && LAYER < (NB_TRACKS + 1))
  {
    if (FADE[LAYER])
    {
      wTrig.trackGain(LAYER, -30);
      wTrig.trackPlayPoly(LAYER);
      wTrig.trackFade(LAYER, 0, FADE_TIME / 2, false);
      wTrig.trackFade(LAYER - 1, -30, FADE_TIME * 1.5, true);
    }

    else wTrig.trackPlaySolo(LAYER);

    delay(WTRIG_VS_LCD_DELAY);

    for (int i = 2; i < 20; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        lcd.setCursor(i, j);
        lcd.print(" ");
      }
    }

    lcd.setCursor(1, 0);
    lcd.print(LAYER);
    lcd.setCursor(4, 0);
    lcd.print(TRACK_LABEL[LAYER - 1]);

    lcd.setCursor(1, 2);
    if(LAYER != NB_TRACKS) lcd.print(LAYER + 1);
    else lcd.print("##");
    lcd.setCursor(4, 2);
    lcd.print(TRACK_LABEL[LAYER]);

    LAST_LAYER = LAYER;
    STOP = false;
  }

  // STOP BUTTON
  // ================================================================================================

  //wTrig.update();

  //  if (!digitalRead(STOP_BUTTON))
  //  {
  //    if (wTrig.isTrackPlaying(LAYER))
  //    {
  //      wTrig.trackStop(LAYER);
  //      delay(33);
  //    }
  //  }

  if (!digitalRead(STOP_BUTTON))
  {
    if (!STOP)
    {
      wTrig.stopAllTracks();

      delay(WTRIG_VS_LCD_DELAY);

      STOP = true;
      LAYER --;
      LAYER = constrain(LAYER, 1, NB_TRACKS);
      LAST_LAYER = LAYER;

      lcd.setCursor(1, 0);
      lcd.print(LAYER);
      lcd.setCursor(4, 0);
      lcd.print(TRACK_LABEL[LAYER - 1]);

      lcd.setCursor(1, 2);
      lcd.print(LAYER + 1);
      lcd.setCursor(4, 2);
      lcd.print(TRACK_LABEL[LAYER]);

      lcd.setCursor(19, 2);
      lcd.print(">");

      lcd.setCursor(4, 3);
      lcd.print("[STOPPED]");

      LAST_STOP_TIME = TIME;
    }

    else if (TIME - LAST_STOP_TIME > SECURITY_DELAY && LAYER != 0)
    {
      LAYER --;
      LAYER = constrain(LAYER, 0, NB_TRACKS);
      LAST_LAYER = LAYER;

      for (int i = 2; i < 20; i++)
      {
        for (int j = 0; j < 4; j++)
        {
          lcd.setCursor(i, j);
          lcd.print(" ");
        }
      }

      lcd.setCursor(1, 0);
      if (LAYER != 0) lcd.print(LAYER);
      else lcd.print("#");

      lcd.setCursor(4, 0);
      if (LAYER != 0) lcd.print(TRACK_LABEL[LAYER - 1]);
      else lcd.print("[TRACK_LABEL]");

      lcd.setCursor(1, 2);
      lcd.print(LAYER + 1);
      lcd.setCursor(4, 2);
      lcd.print(TRACK_LABEL[LAYER]);

      lcd.setCursor(19, 2);
      lcd.print(">");

      LAST_STOP_TIME = TIME;
    }
  }
}
