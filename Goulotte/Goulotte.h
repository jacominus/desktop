#include <AltSoftSerial.h>
#include <wavTrigger.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define DEBUG false

#define PARA_BUTTON 2
#define DESK_BUTTON 4
#define PLAY_BUTTON 7
#define STOP_BUTTON 12

//#define PARA_LED 3
//#define DESK_LED 5
//#define PLAY_LED 6
//#define STOP_LED 11

#define NB_TRACKS 28
#define FADE_TIME 500

#define SECURITY_DELAY 666
#define WTRIG_VS_LCD_DELAY 40

wavTrigger wTrig;
LiquidCrystal_I2C lcd(0x27, 20, 4);

void PLAY_();
void STOP_N_PREVIOUS();

char wTrigVersion[VERSION_STRING_LEN];

unsigned int SD_TRACKS = 0;
unsigned int LAYER = 0;
unsigned int LAST_LAYER = LAYER;

unsigned long TIME;
unsigned long LAST_PLAY_TIME;
unsigned long LAST_STOP_TIME;

boolean TOKENEXT = true;
boolean STOP = false;


// Arrays
// ==================================================================================================


String TRACK_LABEL[NB_TRACKS + 1] = { "Texto-1",         // #1
                                      "Texto-2",         // #2
                                      "Texto-3",         // #3
                                      "Texto-4",         // #4
                                      "Texto-5",         // #5
                                      "Texto-6",         // #6
                                      "Texto-7",         // #7
                                      "Texto-8",         // #8
                                      "Texto-9",         // #9
                                      "Texto-10",        // #10
                                      "Texto-11",        // #11
                                      "Texto-12",        // #12
                                      "Sonnerie",        // #13
                                      "Frederic-1",      // #14
                                      "Frederic-2",      // #15
                                      "Frederic-3",      // #16
                                      "Hooola-1",        // #17
                                      "Cailloux",        // #18
                                      "Machinerie-1",    // #19
                                      "SNCF",            // #20
                                      "Machinerie-2",    // #21
                                      "Choregraphie",    // #22
                                      "Frederic-4",      // #23
                                      "Bizarre",         // #24
                                      "Baroque",         // #25
                                      "Jaconfession",    // #26
                                      "Epilogue",        // #27
                                      "ITW",             // #28
                                      "FIN"              // #29
                                    };


boolean FADE[NB_TRACKS] = { 0,    // #1
                            0,    // #2
                            0,    // #3
                            0,    // #4
                            0,    // #5
                            0,    // #6
                            0,    // #7
                            0,    // #8
                            0,    // #9
                            0,    // #10
                            0,    // #11
                            0,    // #12
                            0,    // #13
                            0,    // #14
                            0,    // #15
                            1,    // #16
                            1,    // #17
                            0,    // #18
                            0,    // #19
                            0,    // #20
                            0,    // #21
                            0,    // #22
                            0,    // #23
                            0,    // #24
                            0,    // #25
                            0,    // #26
                            0,    // #27
                            1     // #28
                          };


// Functions
// ==================================================================================================


//void PLAY_()
//{
//  TIME = millis();
//
//  if (TOKENEXT)
//  {
//    LAYER++;
//    TOKENEXT = false;
//    LAST_PLAY_TIME = TIME;
//  }
//
//  //  else if (!TOKENEXT && TIME - LAST_PLAY_TIME > SECURITY_DELAY)
//  //  {
//  //    TOKENEXT = true;
//  //    lcd.setCursor(19, 2);
//  //    lcd.print(">");
//  //  }
//
//  if (LAYER != LAST_LAYER && LAYER < (NB_TRACKS - 1))
//  {
//    //wTrig.trackPlayPoly(LAYER);
//    wTrig.trackPlaySolo(LAYER);
//
//    LAST_LAYER = LAYER;
//    STOP = false;
//  }
//
//  delay(WTRIG_VS_LCD_DELAY);
//}
//
//
//void STOP_N_PREVIOUS()
//{
//  TIME = millis();
//
//  // Stop function
//  if (!STOP)
//  {
//    //wTrig.trackStop(LAYER);
//    wTrig.stopAllTracks();
//
//    STOP = true;
//  }
//
//  // Previous function
//  if (TIME - LAST_STOP_TIME > SECURITY_DELAY)
//  {
//    LAYER --;
//    LAYER = constrain(LAYER, 1, 29);
//    LAST_LAYER = LAYER;
//
//    LAST_STOP_TIME = TIME;
//  }
//
//  delay(WTRIG_VS_LCD_DELAY);
//}
